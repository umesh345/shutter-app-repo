//This is an example code for Bottom Navigation//
import React, { Component } from 'react';

//import react in our code.
import { SafeAreaView, Text, View, TouchableOpacity, StyleSheet, FlatList, Button } from 'react-native';
import { ListItem } from 'react-native-elements';
//import { List, ListItem} from 'react-native-elements';
//import all the basic component we have used

export default class ScanScreen extends React.Component {
    //Profile Screen to show from Open profile button

    
    state = {

        Data: [],

    };
    
    componentDidMount() {
        this.scanShutter();
    }

    fetchData = () => {
        fetch("http://10.0.0.2:8000/controller/devices/scan", {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    Data: responseJson,

                })

            })
            .catch((error) => {
                console.error(error);
            });

    }


    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this.props.navigation.navigate('Profile',
                {
                    // JSON_ListView_Clicked_Item: item.name
                    name: item.name,
                    mac_address: item.mac_address,
                })}
            title={item.name}
            subtitle={item.mac_address}
            titleStyle={styles.listitemstyle}
            subtitleStyle={styles.listitemstyle}
            bottomDivider
            chevron
        />

    )



    scanShutter = () =>{

        fetch("http://10.0.0.2:8000/controller/devices/scan", {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    Data: responseJson,

                })

            })
            .catch((error) => {
                console.error(error);
            });
        // }


    }

    render() {
        return (
            <View style={styles.MainContainer}>
              
                 <FlatList

                    data={this.state.Data}
                    //extraData={this.state.listHolder}
                    keyExtractor={item => item.mac_address}
                    renderItem={this.renderItem}
               // renderItem={({ item }) => <Text>{item.name},{item.macaddress}</Text>}

                />
            </View>

        )
    }
}


const styles = StyleSheet.create({
    one: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    two: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
    MainContainer: {

        //justifyContent: 'center',
     
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20,
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },

    button: {

        width: '100%',
        height: 40,
        padding: 10,
        backgroundColor: '#000000',
        borderRadius: 8,
        borderColor:'green',
        marginTop: 10
    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});