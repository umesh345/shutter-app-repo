//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { SafeAreaView,Text, View, TouchableOpacity, FlatList, StyleSheet, Button, TextInput } from 'react-native';
import { ListItem } from 'react-native-elements';
//import all the basic component we have used
//import UniqueID from 'react-html-id';

export default class AddShutterScreen extends React.Component {
    //Profile Screen to show from Open profile button
    constructor(props) {
        super(props);
        //this.showShutterlist = this.showShutterlist.bind(this);
        this.state = {
            list: [],
            }
       }
   /*componentDidUpdate() {
        this.showShutterlist();
    }*/
    componentDidMount() {
        this.showShutterlist();
    }

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this.props.navigation.navigate('Details',
                {
                    
                    name: item.name,
                    mac_address: item.mac_address,
                })}
            
            title={item.name}
            subtitle={item.mac_address}
            titleStyle={styles.listitemstyle}
            subtitleStyle={styles.listitemstyle}
            bottomDivider
            chevron
        />

    )

    showShutterlist = () => {
        // componentDidMount = () => {
        fetch('http://10.0.0.2:8000/controller/devices', {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    list: responseJson,

                })

            })
            .catch((error) => {
                console.error(error);
            });
        }

   
       

   
  

    
    
    
  render() {
      
        return (
            <View style={styles.MainContainer} >
                
             
             
            
            <SafeAreaView>
             <FlatList
                    data={this.state.list}
                    //extraData={this.state.listHolder}
                    keyExtractor={item => item.name}
                   // renderItem={({ item }) => <Text> {item.name} ,{item.device}</Text>}
                   renderItem={this.renderItem}
                    />
             </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flex:1,
        backgroundColor: '#add8e6',


    },
    one: {
        color: 'brown',
        fontWeight: 'bold',
        fontSize: 25,
    },
    text: {
        color:'white',
        textAlign:'center',
        fontWeight: 'bold',
        fontSize: 20,
    },
    three: {
        
        borderBottomColor:'black',
        padding: 10,
        alignItems: 'center',
    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold', 
        fontSize: 18,
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
    },

    button: {
        alignItems: 'center',
        backgroundColor: '#000000',
        padding :10,
        marginTop: 16,

    },

    buttonText: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
    logo: {

        height: 180,

        width: 180,

        margin: 60,

    },
});