//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
//import all the basic component we have used
import { ListItem } from 'react-native-elements';

export default class SettingsScreen extends React.Component {
    //Setting Screen to show in Setting Option
    constructor(props) {
        super(props);
        this.showGroup = this.showGroup.bind(this);
       
        this.state = {
            list: [],
           
        }
    }

   


    showGroup = () => {
        // componentDidMount = () => {
        fetch('http://10.0.0.2:8000/controller/devices/list_rooms_only/', {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    list: responseJson,

                })
                
            })
            .catch((error) => {
                console.error(error);
            });
    }

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this.props.navigation.navigate('ScheduleScreen',
                {
                    // JSON_ListView_Clicked_Item: item.name
                    name: item.room,

                })}
            title={item.room}
            //subtitle={item.mac}
            titleStyle={styles.listitemstyle}
            //subtitleStyle={styles.listitemstyle}
            bottomDivider
            chevron
        />

    )

    render() {
        return (
            <View style={styles.MainContainer} >
                <Text style={styles.title}>Schedule </Text>
               
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Home')}>
                    <Text style={styles.text}>Go to Home Tab</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showGroup} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText} >  Show Groups </Text>
                    </TouchableOpacity>
                    <FlatList
                        data={this.state.list}
                        //extraData={this.state.listHolder}
                        keyExtractor={item => item.room}
                        //renderItem={({ item }) => <Text> {item.room} </Text>}
                        renderItem={this.renderItem}
                    />
                    
               
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },
    title: {
        color: 'black',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
    },
    text: {
        color:'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    button: {

        height: 40,
        padding: 10,
        backgroundColor: '#000000',
        borderRadius: 8,
        marginTop: 10,
        borderWidth: 2,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});