//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet, Button, Alert, FlatList,SafeAreaView,TextInput } from 'react-native';


//import all the basic component we have used




export default class ScheduleScreen extends React.Component {
    //Profile Screen to show from Open profile button
    constructor(props) {
        super(props);
        this.schedulegroup = this.schedulegroup.bind(this);
        this.state = {
            shuttername: '',
            position: '',
            min:'',
            hour: '',
            day: '',
            date: '',
            month:'',

        }
    }
    handletext = (text) => {
        this.setState({ position: text });

    }
    handletext1 = (text) => {
        this.setState({ min: text });

    }
    handletext2 = (text) => {
        this.setState({ hour: text });

    }
    handletext3 = (text) => {
        this.setState({ day: text });

    }
    handletext4 = (text) => {
        this.setState({ date: text });

    }
    handletext5 = (text) => {
        this.setState({ month: text });

    }
    schedulegroup = (name,position,min,hour,day,date,month) => {
        const url = 'http://10.0.0.2:8000/controller/devices/schedule/' + name + '/' + position + '/' + min
            + '/' + hour + '/' + day + '/' + date + '/' + month;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

   render() {
       const { navigation } = this.props;
       const name = navigation.getParam('name', '');
       return (
           <View style={styles.MainContainer}>
               <View style={styles.container}>
                   <Text style={styles.text}> Schedule Screen </Text>
               </View>
               <View style={{ padding: 20 }}>
                   <Text style={styles.text}>name: {JSON.stringify(name)}</Text>
               </View>
               <TextInput
                   placeholder='position' style={styles.textInputStyle}
                   underlineColorAndroid='transparent'
                   onChangeText={this.handletext}
               /> 
                 <TextInput
                   placeholder='minute' style={styles.textInputStyle}
               underlineColorAndroid='transparent'
                   onChangeText={this.handletext1}
             
               />
               <TextInput
                   placeholder='hours' style={styles.textInputStyle}
                   underlineColorAndroid='transparent'
                   onChangeText={this.handletext2}

               />
               <TextInput
                   placeholder='day of the week' style={styles.textInputStyle}
                   underlineColorAndroid='transparent'
                   onChangeText={this.handletext3}

               />
               <TextInput
                   placeholder='date' style={styles.textInputStyle}
                   underlineColorAndroid='transparent'
                   onChangeText={this.handletext4}

               />
               <TextInput
                   placeholder='month' style={styles.textInputStyle}
                   underlineColorAndroid='transparent'
                   onChangeText={this.handletext5}

               />
               <TouchableOpacity onPress={() => this.schedulegroup(name, this.state.position,
                   this.state.min, this.state.hour, this.state.day, this.state.date,
                this.state.month)} activeOpacity={0.7} style={styles.button} >
                   <Text style={styles.buttonText}>  Move Shutter </Text>
               </TouchableOpacity>
           </View>
       );
    }
}

const styles = StyleSheet.create({
    text: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    container: {
        alignItems: 'center',
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
    MainContainer: {


        flex: 1,
        margin: 2,
         backgroundColor: '#add8e6',
    },
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20,
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '100%',
        borderWidth: 1,
        borderColor: 'blue',
        borderRadius: 20,
        marginTop: 12
    },

    button: {

        alignItems: 'center',

        backgroundColor: '#000000',

        padding: 10,

        width: 300,

        marginTop: 16,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});