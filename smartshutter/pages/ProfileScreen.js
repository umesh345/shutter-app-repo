//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet,Button,Alert } from 'react-native';

//import all the basic component we have used




export default class ProfileScreen extends React.Component {
    //Profile Screen to show from Open profile button
    constructor(props) {
        super(props);
        this.addShutter = this.addShutter.bind(this);
    }

    addShutter = (name, mac_address) => {
        const url = 'http://10.0.0.2:8000/controller/devices/add/' + name + '/' + mac_address;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert("Shutter added");


            })
            .catch((error) => {
                console.error(error);
            });
    }

  
    

   /* addShutter = (name,macad) => {
        fetch('http://192.168.1.88:8000/controller/devices/add/', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                firstParam: name,
                secondParam: macad,
            }),
        });
    }*/


    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('name', '');
        const mac_address = navigation.getParam('mac_address', 'default');
        return (
            <View style={styles.MainContainer}>
                <View style={styles.container}>
                    <Text style={styles.text}>Profile Screen </Text>
                </View>
                <View style={{padding : 20 }}>
                <Text   style={styles.text}>name: {JSON.stringify(name)}</Text>
                <Text style={styles.text}>mac_address: {JSON.stringify(mac_address)}</Text>
                    <TouchableOpacity onPress={() => this.addShutter(name,mac_address)} activeOpacity={0.7} style={styles.button} >
                    <Text style={styles.buttonText}>  Add Shutter </Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    container: {
        alignItems: 'center',
     },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
   
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20,
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
    },
    MainContainer: {
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },

    button: {

        alignItems: 'center',

        backgroundColor: '#000000',

        padding: 10,

        width: 300,

        marginTop: 16,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});