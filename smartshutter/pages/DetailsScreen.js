//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet, Button, Alert, TextInput, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements';
//import all the basic component we have used




export default class DetailsScreen extends React.Component {
    //Profile Screen to show from Open profile button
    constructor(props) {
        super(props);
        this.testmsg = this.testmsg.bind(this);
        this.removeShutter = this.removeShutter.bind(this);
        this.moveShutter = this.moveShutter.bind(this);
        this.shutterposition = this.shutterposition.bind(this);
        this.showGroup = this.showGroup.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.state = {
            list : [],
            inputtext: '',
            groupname:'',
        }
    }

    removeShutter = (name) => {
        const url = 'http://10.0.0.2:8000/controller/devices/remove/'+name ;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

    handletext = (text) => {
        this.setState({ inputtext: text });
        
    }

    moveShutter = (name,position) => {
        const url = 'http://10.0.0.2:8000/controller/' + name+'/move/'+position;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

    shutterposition = (name) => {
        const url = 'http://10.0.0.2:8000/controller/'+name+'/position';
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

    showGroup = () => {
        // componentDidMount = () => {
        fetch('http://10.0.0.2:8000/controller/devices/list_rooms_only/', {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    list: responseJson,

                })
               
            })
            .catch((error) => {
                console.error(error);
            });
    }
    addGroup = (name,groupname) => {
        const url = 'http://10.0.0.2:8000/controller/devices/add_to_room/' + groupname+'/' + name;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

    testmsg = (item) => {
        this.setState({ groupname: item.room });
        Alert.alert('Group Selection',item.room + ' is selected');
    }

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this.testmsg(item)}
           
            title={item.room}
            //subtitle={item.mac}
            titleStyle={styles.listitemstyle}
            //subtitleStyle={styles.listitemstyle}
            bottomDivider
            chevron
        />

    )
  


    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('name', '');
        const mac_address = navigation.getParam('mac_address', 'default');
        return (
            <View style={styles.MainContainer}>
                <View style={styles.container}>
                    <Text style={styles.text}>Shutter Detail </Text>
                </View>
                <View style={{ padding: 20 }}>
                    <Text style={styles.text}>name: {JSON.stringify(name)}</Text>
                    <Text style={styles.text}>mac_address: {JSON.stringify(mac_address)}</Text>
                    <TouchableOpacity onPress={() => this.removeShutter(name)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Remove Shutter </Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.shutterposition(name)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Current Shutter Position </Text>
                    </TouchableOpacity>

                    <TextInput
                        placeholder='Shutter Position' style={styles.textInputStyle}
                        underlineColorAndroid='transparent'
                        //defaultValue={this.state.value}
                        //onChangeText={(text) => this.setState({ text })}
                        onChangeText={this.handletext}
                    //value={this.state.text}
                    //onChangeText={data => this.setState({ groupname: data })}
                    />
                    <TouchableOpacity onPress={() => this.moveShutter(name,this.state.inputtext)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Move Shutter </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showGroup} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText} >Select Group </Text>
                    </TouchableOpacity>
                    <FlatList
                        data={this.state.list}
                        //extraData={this.state.listHolder}
                        keyExtractor={item => item.room}
                        //renderItem={({ item }) => <Text> {item.room} </Text>}
                        renderItem={this.renderItem}
                    />
                    <TouchableOpacity onPress={() => this.addGroup(name, this.state.groupname)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Add to Group </Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    container: {
        alignItems: 'center',
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
    MainContainer: {
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20,
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 20,
        marginTop: 10,
    },

    button: {

        width: '90%',
        height: 40,
        padding: 10,
        backgroundColor: 'black',
        borderRadius: 8,
        borderColor:'black',
        marginTop: 10,
        
    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});