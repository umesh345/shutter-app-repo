//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet,} from 'react-native';
//import all the basic component we have used

export default class HomeScreen extends React.Component {
  //Home Screen to show in Home Option

  render() {
    return (
      <View style={styles.MainContainer}>
            <Text style={styles.texttitle}> Smart Shutter </Text>
            
            <View style={styles.container}>
               
                    
            <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('ScanShutter')}>
                    <Text style={styles.text}> Scan Device </Text>
                </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
                    onPress={() => this.props.navigation.navigate('AddShutter')}>
                    <Text style={styles.text}> Shutter List</Text>
          </TouchableOpacity>
                
          <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('Group')}>
                    <Text style={styles.text}>Group </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('test')}>
                    <Text style={styles.text}>TEST SCREEN </Text>
                </TouchableOpacity>
                

            </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    MainContainer: {
       flex:1,
        margin: 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#add8e6',


    },
    container: {
        flex:1,
        margin: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#add8e6',


    },
    texttitle: {
        marginTop: 20,
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25,
    },
    text: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
    },
    button: {

        alignItems: 'center',

        backgroundColor: '#000000',

        padding: 10,

        width: 300,

        marginTop: 16,

    },
});