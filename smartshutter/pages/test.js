

//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet, FlatList, Image } from 'react-native';
//import all the basic component we have used
import { ListItem } from 'react-native-elements';

export default class test extends React.Component {
    //Setting Screen to show in Setting Option
    constructor(props) {
        super(props);
        

        this.state = {
            time: '10:00',

        }
    }

    

   

  

    render() {
        return (
            <View>
                <Text style={styles.title}>TEST SCREEN </Text>

                <View>
               
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MainContainer: {
        //justifyContent: 'center',
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },
    title: {
        color: 'black',
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
    },
    text: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    button: {

        height: 40,
        padding: 10,
        backgroundColor: '#000000',
        borderRadius: 8,
        marginTop: 10,
        borderWidth: 2,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});