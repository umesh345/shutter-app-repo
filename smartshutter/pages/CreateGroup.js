//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, FlatList, StyleSheet, Button, TextInput, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';

export default class CreateGroup extends React.Component {
    constructor(props) {
        super(props);
        this.createGroup = this.createGroup.bind(this);
        this.state = {
            list: [],
            inputtext: '',
        }
    }

    handletext = (text) => {
        this.setState({ inputtext: text })
    }

    createGroup = (text) => {
        const url = 'http://10.0.0.2:8000/controller/devices/create_group/' + text;
        fetch(url)
            .then((response) => response)
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert("Group is created");


            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        return (
            <View style={styles.MainContainer}>
                <Text style={styles.text}>Create group screen</Text>
                
                <TextInput
                    placeholder='Groupname' style={styles.textInputStyle}
                    underlineColorAndroid='transparent'
                    //defaultValue={this.state.value}
                    //onChangeText={(text) => this.setState({ text })}
                    onChangeText={this.handletext}
                    //value={this.state.text}
                    //onChangeText={data => this.setState({ groupname: data })}
                />
               
                <TouchableOpacity onPress={() => this.createGroup(this.state.inputtext)} activeOpacity={0.7} style={styles.button} >
                    <Text style={styles.buttonText} >  create Groups </Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    one: {
        color: 'brown',
        fontWeight: 'bold',
        fontSize: 25,
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 20,
        marginBottom:20,
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
    MainContainer: {
    alignItems: 'center',
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '100%',
        borderWidth: 5,
        borderColor: 'blue',
        borderRadius: 7,
        marginTop: 12
    },

    button: {

        alignItems: 'center',

        backgroundColor: '#000000',

        padding: 10,

        width: '100%',

        marginTop: 16,

    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});