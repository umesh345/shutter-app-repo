//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, FlatList, StyleSheet, Button, TextInput } from 'react-native';
import { ListItem } from 'react-native-elements';
//import all the basic component we have used
//import UniqueID from 'react-html-id';

export default class GroupScreen extends React.Component {
    //Profile Screen to show from Open profile button
    constructor(props) {
        super(props);
        
        this.state = {
            list: [],
            groupList: [],
            groupname : '',          
        }
    }



    render() {
        
        return (
            <View style={styles.MainContainer}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('ShowGroup')}>
                    <Text style={styles.buttonText}> Show Group </Text>
                </TouchableOpacity>
               
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('CreateGroup')}>
                    <Text style={styles.buttonText}> Create Group </Text>
                </TouchableOpacity>
               
               
            </View>
        )

    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        margin: 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#add8e6',


    },
    one: {
        color: 'brown',
        fontWeight: 'bold',
        fontSize: 25,
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
   

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
    },

    button: {
        alignItems: 'center',
        backgroundColor: '#000000',
        height: 40,
        padding: 10,
        borderRadius: 8,
        marginTop: 10,
        borderWidth: 2,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});