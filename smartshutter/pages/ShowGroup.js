import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, FlatList, StyleSheet, Button, TextInput } from 'react-native';
import { ListItem } from 'react-native-elements';

export default class ShowGroup extends React.Component {
    constructor(props) {
        super(props);
       //this.showGroup = this.showGroup.bind(this);
        this.state = {
            list: [],
            groupList: [],
            groupname: '',
        }
    }
    componentDidUpdate() {
        this.showGroup();
    }
    componentDidMount() {
        this.showGroup();
    }

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this.props.navigation.navigate('GroupDetail',
                {
                    // JSON_ListView_Clicked_Item: item.name
                    name: item.room,

                })}
            //leftAvatar={{ source: { uri: item.device } }}
            title={item.room}
            //subtitle={item.mac}
            titleStyle={styles.listitemstyle}
            //subtitleStyle={styles.listitemstyle}
            bottomDivider
            chevron
        />

    )

    showGroup = () => {
        
        fetch('http://10.0.0.2:8000/controller/devices/list_rooms_only/', {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    list: responseJson,

                })
                
            })
            .catch((error) => {
                console.error(error);
            });
         }


    render() {
        return (
            <View style={styles.MainContainer}>
                <Text>Show group screen!</Text>


                
                <FlatList
                    data={this.state.list}
                   
                    keyExtractor={item => item.room}
                    //renderItem={({ item }) => <Text> {item.room} </Text>}
                    renderItem={this.renderItem}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    one: {
        color: 'brown',
        fontWeight: 'bold',
        fontSize: 25,
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
    MainContainer: {

        justifyContent: 'center',
        
        flex: 1,
        margin: 2,
        backgroundColor: '#add8e6',

    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
    },

    button: {

        height: 40,
        padding: 10,
        backgroundColor: '#000000',
        borderRadius: 8,
        marginTop: 10,
        borderWidth: 2,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});