//This is an example code for Bottom Navigation//
import React from 'react';
//import react in our code.
import { Text, View, TouchableOpacity, StyleSheet, Button, Alert, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements';

//import all the basic component we have used




export default class GroupDetailScreen extends React.Component {
    //Profile Screen to show from Open profile button
    constructor(props) {
        super(props);
        //this.showShutter = this.showShutter.bind(this);
        this.testmsg = this.testmsg.bind(this);
        this.removeRoom = this.removeRoom.bind(this);
        this.removefromgroup = this.removefromgroup.bind(this);
        this.state = {
            list: [],
            shuttername:'',
        }
    }

    testmsg = (item) => {
        this.setState({ shuttername: item.name });
        Alert.alert('Shutter Selection', item.name + ' is selected');
    }

    renderItem = ({ item }) => (
        <ListItem
            onPress={() => this.testmsg(item)}
            title={item.name}
            subtitle={item.mac_address}
            titleStyle={styles.listitemstyle}
            subtitleStyle={styles.listitemstyle}
            bottomDivider
            chevron
        />

    )

    showShutter = (name) => {
        // componentDidMount = () => {
        fetch('http://10.0.0.2:8000/controller/devices/list_rooms_shutters/'+name, {
            method: 'Get'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    list: responseJson,

                })

            })
            .catch((error) => {
                console.error(error);
            });
    }

    removefromgroup = (name,shuttername) => {
        const url = 'http://10.0.0.2:8000/controller/devices/remove_from_room/' + name + '/' + shuttername;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

    removeRoom = (name) => {
        const url = 'http://10.0.0.2:8000/controller/devices/remove_room/'+name ;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson));
                Alert.alert(JSON.stringify(responseJson));


            })
            .catch((error) => {
                console.error(error);
            });
    }

   


    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('name', '');
        
        return (
            <View style={styles.MainContainer}>
                <View style={styles.container}>
                    <Text style={styles.text}> Group Detail Screen </Text>
                </View>
                <View style={{ padding: 20 }}>
                    <Text style={styles.text}>name: {JSON.stringify(name)}</Text>
                   
                    <TouchableOpacity onPress={() => this.showShutter(name)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Show Shutter </Text>
                    </TouchableOpacity>

                    <FlatList
                        data={this.state.list}
                        //extraData={this.state.listHolder}
                        keyExtractor={item => item.name}
                        //renderItem={({ item }) => <Text> {item.name} </Text>}
                        renderItem={this.renderItem}
                    />
                    <TouchableOpacity onPress={() => this.removefromgroup(name,this.state.shuttername)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Remove Shutter </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.removeRoom(name)} activeOpacity={0.7} style={styles.button} >
                        <Text style={styles.buttonText}>  Remove Group </Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },
    container: {
        alignItems: 'center',
    },
    three: {

        borderBottomColor: 'black',
        padding: 10,
        alignItems: 'center',
    },
    MainContainer: {


        flex: 1,
        margin: 2

    },
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20,
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
    listitemstyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18,
    },

    textInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '90%',
        borderWidth: 1,
        borderColor: '#4CAF50',
        borderRadius: 7,
        marginTop: 12
    },

    button: {

        alignItems: 'center',

        backgroundColor: '#000000',

        padding: 10,

        width: 300,

        marginTop: 16,

    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
    },
});