//This is an example code for Bottom Navigation//
import React from 'react';
import { Button, Text, View, TouchableOpacity, StyleSheet } from 'react-native';
//import all the basic component we have used
import Ionicons from 'react-native-vector-icons/Ionicons';
//import Ionicons to show the icon for bottom options

//For React Navigation 3+
//import {
//  createStackNavigator,
//  createBottomTabNavigator,
//  createAppContainer,
//} from 'react-navigation';

//For React Navigation 4+
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './pages/HomeScreen';
import ScheduleScreen from './pages/ScheduleScreen';
import ScanScreen from './pages/ScanScreen';
import GroupDetail from './pages/GroupDetail';
import ShowGroup from './pages/ShowGroup';
import CreateGroup from './pages/CreateGroup';
import SettingsScreen from './pages/SettingsScreen';
import DetailsScreen from './pages/DetailsScreen';
import ProfileScreen from './pages/ProfileScreen';
import GroupScreen from './pages/GroupScreen';
import AddShutterScreen from './pages/AddShutterScreen';
import test from './pages/test';
const HomeStack = createStackNavigator(
    {
        //Defination of Navigaton from home screen
        Home: { screen: HomeScreen },
        ScheduleScreen: { screen: ScheduleScreen },
        Details: { screen: DetailsScreen },
        GroupDetail: {screen:GroupDetail},
        ScanShutter: { screen: ScanScreen },
        ShowGroup: { screen: ShowGroup },
        CreateGroup: {screen:CreateGroup},
        Profile: { screen: ProfileScreen },
        Group: { screen: GroupScreen },
        AddShutter: { screen: AddShutterScreen },
        test: { screen: test},
    },
    {
        defaultNavigationOptions: {

            //Header customization of the perticular Screen

            title: 'Blink Shutters',



            headerTintColor: '#FFFFFF',

            headerTitleStyle: {

                fontWeight: 'bold',

                fontSize: 25,

                fontFamily: 'Futura',
                

            },

            headerStyle: {

                backgroundColor: 'blue',
               

            },





        },
    }
);
const SettingsStack = createStackNavigator(
    {
        //Defination of Navigaton from setting screen
        Settings: { screen: SettingsScreen },
        Schedule: { screen: ScheduleScreen },
        Details: { screen: DetailsScreen },
        Profile: { screen: ProfileScreen },
    },
    {
        defaultNavigationOptions: {
            //Header customization of the perticular Screen
            headerStyle: {
                backgroundColor: '#42f44b',
            },
            headerTintColor: '#FFFFFF',
            title: 'Settings',
            //Header title
        },
    }
);
const App = createBottomTabNavigator(

    {

        Home: {

            screen: HomeStack,

            navigationOptions: {

                tabBarIcon: ({ tintColor }) => (

                    <Ionicons name='ios-add' color={tintColor} size={25} />

                ),

            },



        },

        /*AddShutter: {

            screen: ScanScreen,

            navigationOptions: {

                title: "Search Shutters",



                tabBarVisible: false,

                tabBarIcon: ({ tintColor }) => (

                    <View

                        style={{

                            position: 'absolute',

                            bottom: 20, // space from bottombar

                            height: 58,

                            width: 58,

                            borderRadius: 58,

                            backgroundColor: '#000000',

                            justifyContent: 'center',

                            alignItems: 'center',

                        }}>

                        <Ionicons name='ios-add' color={tintColor} size={40} />

                    </View>

                ),

            },

        },*/

        Schedule: {

            screen: SettingsScreen,

            navigationOptions: {

                tabBarIcon: ({ tintColor }) => (

                    <View>

                        <Ionicons name='ios-add' color={tintColor} size={25} />

                    </View>

                ),

            },



        },

    },

    {



        initialRouteName: 'Home',

        tabBarOptions: {

            activeTintColor: 'yellow',

            inactiveTintColor: '#fff',

            showIcon: true,

            style: {

                backgroundColor: 'blue',

            }

        },

    }

);

export default createAppContainer(App);